/* ============================================================
*
* This file is a part of the QtDemoBrowser project
*
* Copyright (C) 2011 by Rohan Garg <rohan16garg@gmail.com>
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License or (at your option) version 3 or any later version
* accepted by the membership of KDE e.V. (or its successor approved
* by the membership of KDE e.V.), which shall act as a proxy
* defined in Section 14 of version 3 of the license.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* ============================================================ */
#include "MainWindow.h"
#include <QGraphicsWebView>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLineEdit>
#include <QGraphicsProxyWidget>
#include <QGraphicsLinearLayout>
#include <QGraphicsWidget>
#include <QtGui>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    scene = new QGraphicsScene(this);
    urlbar = new QLineEdit;

    view = new QGraphicsView(scene);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setCentralWidget(view);

    QGraphicsProxyWidget *proxywidget = scene->addWidget(urlbar);
    webview = new QGraphicsWebView;

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout(Qt::Vertical);
    layout->addItem(proxywidget);
    layout->addItem(webview);

//    webview->load(QUrl("http://www.google.com"));

    form = new QGraphicsWidget;
    scene->addItem(form);
    form->setLayout(layout);
    form->resize(this->size());

    view->setFrameShape(QFrame::NoFrame);

    this->show();

    connect(urlbar, SIGNAL(editingFinished()), this,SLOT(loadurl()));
}

MainWindow::~MainWindow()
{

}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    view->resize(event->size());
    form->resize(event->size());
    QMainWindow::resizeEvent(event);
}

void MainWindow::loadurl()
{
    webview->load(QUrl(urlbar->text()));
    urlbar->setText(webview->url().toString());
}
