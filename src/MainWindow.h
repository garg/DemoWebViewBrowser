/* ============================================================
*
* This file is a part of the QtDemoBrowser project
*
* Copyright (C) 2011 by Rohan Garg <rohan16garg@gmail.com>
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License or (at your option) version 3 or any later version
* accepted by the membership of KDE e.V. (or its successor approved
* by the membership of KDE e.V.), which shall act as a proxy
* defined in Section 14 of version 3 of the license.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* ============================================================ */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>

class QGraphicsScene;
class QGraphicsView;
class QGraphicsWebView;
class QGraphicsWidget;
class QLineEdit;

class MainWindow : public QMainWindow
{
    Q_OBJECT
private: QGraphicsScene *scene;
         QGraphicsView *view;
         QGraphicsWebView *webview;
         QLineEdit *urlbar;
         QGraphicsWidget *form;
public:
    MainWindow(QWidget *parent = 0);
    void resizeEvent(QResizeEvent *event);
    ~MainWindow();

public slots:
        void loadurl();
};

#endif // MAINWINDOW_H

